# Oregano Sync - The Odoo Outlook Connector Add-In

The Outlook add-in allows the user to connect
Odoo and Outlook. Synchronization is bi-directional
and the user may decide which direction to prioritize
(Odoo or Outlook).

## Software Versions

### Outlook

The add-in is tested to work with Outlook 2010,
Outlook 2013, and Outlook 2016.

### Odoo

The add-in is tested to work with Odoo 8.0, Odoo 9.0, 
and Odoo 10.0.

## Features

### Add-In Ribbon
![Add-In Ribbon](ribbon.PNG)

### Connection Settings
![Connection Settings](folders.PNG)

### Additional Settings
![Additional Settings](settings.PNG)

### Attach E-mails to Models in Odoo as Messages

Add e-mail content to:

*  Leads
*  Opportunities
*  Projects
*  Tasks
*  Partners

#### Attach E-mails to Models in Odoo as Messages
![Attach E-mails to Models in Odoo as Messages](attach1.png)

#### Search for Objects in Models
![Search for Objects in Models](attach2.png)

#### E-mail in Odoo as Message
![E-mail in Odoo as Message](attach3.png)


# Technical Side

## Installer

This add-in registers itself as a Component Object
Model (COM) class in the Windows registry to appear
in Outlook. The installer uses the [Nullsoft Scriptable
Install System (NSIS)](https://nsis.sourceforge.io/Main_Page).

## NetOffice

For accessing Outlook the [NetOffice .Net Assemblies](https://osdn.net/projects/netoffice/) are used.
They are provided in the NetOffice folder.

## Compilation steps

1.  Open the `OutlookAddIn.sln` solution in Microsoft Visual Studio 2013
2.  Build the project in Debug mode
3.  Compile the `nsis_install_script.nsi` file using NSIS
4.  Run the `OutlookAddInInstaller.exe` executable

For ease of use a compiled 32-bit `OutlookAddInInstaller.exe`
is provided in the repository.
