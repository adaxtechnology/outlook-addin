﻿namespace OdooOutlookAddIn
{
    partial class connect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(connect));
            this.serverText = new System.Windows.Forms.TextBox();
            this.usernameText = new System.Windows.Forms.TextBox();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.databaseText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.odooSettings = new System.Windows.Forms.TabControl();
            this.connection = new System.Windows.Forms.TabPage();
            this.settings = new System.Windows.Forms.TabPage();
            this.autoUpdate = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sync1h = new System.Windows.Forms.RadioButton();
            this.sync15Mins = new System.Windows.Forms.RadioButton();
            this.syncManual = new System.Windows.Forms.RadioButton();
            this.syncSettings = new System.Windows.Forms.GroupBox();
            this.outlookPrio = new System.Windows.Forms.RadioButton();
            this.odooPrio = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.odooSettings.SuspendLayout();
            this.connection.SuspendLayout();
            this.settings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.syncSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // serverText
            // 
            this.serverText.Location = new System.Drawing.Point(6, 30);
            this.serverText.Name = "serverText";
            this.serverText.Size = new System.Drawing.Size(178, 20);
            this.serverText.TabIndex = 0;
            // 
            // usernameText
            // 
            this.usernameText.Location = new System.Drawing.Point(6, 128);
            this.usernameText.Name = "usernameText";
            this.usernameText.Size = new System.Drawing.Size(178, 20);
            this.usernameText.TabIndex = 2;
            // 
            // passwordText
            // 
            this.passwordText.Location = new System.Drawing.Point(6, 181);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.Size = new System.Drawing.Size(178, 20);
            this.passwordText.TabIndex = 3;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(12, 286);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(194, 23);
            this.saveBtn.TabIndex = 5;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 105);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Password";
            // 
            // databaseText
            // 
            this.databaseText.Location = new System.Drawing.Point(6, 77);
            this.databaseText.Name = "databaseText";
            this.databaseText.Size = new System.Drawing.Size(178, 20);
            this.databaseText.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Database";
            // 
            // odooSettings
            // 
            this.odooSettings.Controls.Add(this.connection);
            this.odooSettings.Controls.Add(this.settings);
            this.odooSettings.Location = new System.Drawing.Point(12, 12);
            this.odooSettings.Name = "odooSettings";
            this.odooSettings.SelectedIndex = 0;
            this.odooSettings.Size = new System.Drawing.Size(198, 268);
            this.odooSettings.TabIndex = 10;
            // 
            // connection
            // 
            this.connection.Controls.Add(this.serverText);
            this.connection.Controls.Add(this.label4);
            this.connection.Controls.Add(this.usernameText);
            this.connection.Controls.Add(this.databaseText);
            this.connection.Controls.Add(this.passwordText);
            this.connection.Controls.Add(this.label3);
            this.connection.Controls.Add(this.label2);
            this.connection.Controls.Add(this.label1);
            this.connection.Location = new System.Drawing.Point(4, 22);
            this.connection.Name = "connection";
            this.connection.Padding = new System.Windows.Forms.Padding(3);
            this.connection.Size = new System.Drawing.Size(190, 242);
            this.connection.TabIndex = 1;
            this.connection.Text = "Connection";
            this.connection.UseVisualStyleBackColor = true;
            // 
            // settings
            // 
            this.settings.Controls.Add(this.autoUpdate);
            this.settings.Controls.Add(this.groupBox1);
            this.settings.Controls.Add(this.syncSettings);
            this.settings.Location = new System.Drawing.Point(4, 22);
            this.settings.Name = "settings";
            this.settings.Padding = new System.Windows.Forms.Padding(3);
            this.settings.Size = new System.Drawing.Size(190, 242);
            this.settings.TabIndex = 2;
            this.settings.Text = "Settings";
            this.settings.UseVisualStyleBackColor = true;
            // 
            // autoUpdate
            // 
            this.autoUpdate.AutoSize = true;
            this.autoUpdate.Location = new System.Drawing.Point(16, 215);
            this.autoUpdate.Name = "autoUpdate";
            this.autoUpdate.Size = new System.Drawing.Size(155, 17);
            this.autoUpdate.TabIndex = 2;
            this.autoUpdate.Text = "Perform Automatic Updates";
            this.autoUpdate.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sync1h);
            this.groupBox1.Controls.Add(this.sync15Mins);
            this.groupBox1.Controls.Add(this.syncManual);
            this.groupBox1.Location = new System.Drawing.Point(6, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 93);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Synchronization Frequency";
            // 
            // sync1h
            // 
            this.sync1h.AutoSize = true;
            this.sync1h.Location = new System.Drawing.Point(10, 65);
            this.sync1h.Name = "sync1h";
            this.sync1h.Size = new System.Drawing.Size(87, 17);
            this.sync1h.TabIndex = 2;
            this.sync1h.TabStop = true;
            this.sync1h.Text = "Every 1 Hour";
            this.sync1h.UseVisualStyleBackColor = true;
            // 
            // sync15Mins
            // 
            this.sync15Mins.AutoSize = true;
            this.sync15Mins.Location = new System.Drawing.Point(10, 42);
            this.sync15Mins.Name = "sync15Mins";
            this.sync15Mins.Size = new System.Drawing.Size(107, 17);
            this.sync15Mins.TabIndex = 1;
            this.sync15Mins.TabStop = true;
            this.sync15Mins.Text = "Every 15 Minutes";
            this.sync15Mins.UseVisualStyleBackColor = true;
            this.sync15Mins.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // syncManual
            // 
            this.syncManual.AutoSize = true;
            this.syncManual.Location = new System.Drawing.Point(10, 19);
            this.syncManual.Name = "syncManual";
            this.syncManual.Size = new System.Drawing.Size(60, 17);
            this.syncManual.TabIndex = 0;
            this.syncManual.TabStop = true;
            this.syncManual.Text = "Manual";
            this.syncManual.UseVisualStyleBackColor = true;
            // 
            // syncSettings
            // 
            this.syncSettings.Controls.Add(this.outlookPrio);
            this.syncSettings.Controls.Add(this.odooPrio);
            this.syncSettings.Controls.Add(this.label5);
            this.syncSettings.Location = new System.Drawing.Point(6, 6);
            this.syncSettings.Name = "syncSettings";
            this.syncSettings.Size = new System.Drawing.Size(178, 106);
            this.syncSettings.TabIndex = 0;
            this.syncSettings.TabStop = false;
            this.syncSettings.Text = "Synchronization Setting";
            // 
            // outlookPrio
            // 
            this.outlookPrio.AutoSize = true;
            this.outlookPrio.Location = new System.Drawing.Point(10, 79);
            this.outlookPrio.Name = "outlookPrio";
            this.outlookPrio.Size = new System.Drawing.Size(104, 17);
            this.outlookPrio.TabIndex = 2;
            this.outlookPrio.TabStop = true;
            this.outlookPrio.Text = "Prioritize Outlook";
            this.outlookPrio.UseVisualStyleBackColor = true;
            // 
            // odooPrio
            // 
            this.odooPrio.AutoSize = true;
            this.odooPrio.Location = new System.Drawing.Point(10, 59);
            this.odooPrio.Name = "odooPrio";
            this.odooPrio.Size = new System.Drawing.Size(93, 17);
            this.odooPrio.TabIndex = 1;
            this.odooPrio.TabStop = true;
            this.odooPrio.Text = "Prioritize Odoo";
            this.odooPrio.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 39);
            this.label5.TabIndex = 0;
            this.label5.Text = "Select whether this add-in should\r\nprioritize information found in \r\nOdoo or in O" +
    "utlook.";
            // 
            // connect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 318);
            this.Controls.Add(this.odooSettings);
            this.Controls.Add(this.saveBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "connect";
            this.Text = "Connection Settings";
            this.odooSettings.ResumeLayout(false);
            this.connection.ResumeLayout(false);
            this.connection.PerformLayout();
            this.settings.ResumeLayout(false);
            this.settings.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.syncSettings.ResumeLayout(false);
            this.syncSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox serverText;
        private System.Windows.Forms.TextBox usernameText;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox databaseText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl odooSettings;
        private System.Windows.Forms.TabPage connection;
        private System.Windows.Forms.TabPage settings;
        private System.Windows.Forms.GroupBox syncSettings;
        private System.Windows.Forms.RadioButton outlookPrio;
        private System.Windows.Forms.RadioButton odooPrio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton sync15Mins;
        private System.Windows.Forms.RadioButton syncManual;
        private System.Windows.Forms.RadioButton sync1h;
        private System.Windows.Forms.CheckBox autoUpdate;
    }
}