﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetOffice;
using NetOffice.Tools;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using NetOffice.OutlookApi.Tools;
using Office = NetOffice.OfficeApi;
using NetOffice.OfficeApi.Enums;
using NetOffice.OfficeApi.Tools;
using odooWebservice;
using CookComputing.XmlRpc;
using System.Collections;
using System.IO;
using Microsoft.Win32;
using odooOutlookSync;

namespace OdooOutlookAddIn
{
    public partial class Attach2Lead : Form
    {
        private Addin parentWindow;
        private Outlook.MailItem mailItem;
        private Dictionary<string, object[]> records;

        public Attach2Lead(Outlook.MailItem mailItem, Addin parentWindow)
        {
            this.parentWindow = parentWindow;
            this.mailItem = mailItem;

            InitializeComponent();
        }

        // We download all the records at startup so that the form doesn't lag when choosing a model
        private void getRecords()
        {
            odooWebserviceConnector odooConnection;
            try
            {
                odooConnection = new odooWebserviceConnector();
            }
            catch
            {
                MessageBox.Show("Login to Odoo server failed or connection isn't configured yet. Check configuration in connection setup.");
                this.Close();
                return;
            }
            var models = new ModelInfo[this.model.Items.Count];
            this.model.Items.CopyTo(models, 0); // copy it so we may remove unwanted models from original
            foreach (ModelInfo model in models)
            {
                try
                {
                    object[] argsList = new object[] { model.domain };
                    XmlRpcStruct argsDict= new XmlRpcStruct();
                    argsDict.Add("fields", new object[] { "id", model.displayField });
                    object[] modelRecords = (object[])odooConnection.execute_kw(model.modelName, "search_read", argsList, argsDict);

                    this.records.Add(model.displayName, modelRecords);
                }
                catch (Exception)
                {
                    this.model.Items.Remove(model);
                    Logger.Log("Model unavailable", model.modelName);
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.model.Items.Add(new ModelInfo("project.project", "name", "Projects"));
            this.model.Items.Add(new ModelInfo("crm.lead", "name", "Leads", new object[] { new object[] { "type", "=", "lead" } }));
            this.model.Items.Add(new ModelInfo("crm.lead", "name", "Opportunities", new object[] { new object[] { "type", "=", "opportunity" } }));
            this.model.Items.Add(new ModelInfo("res.partner", "name", "Partners"));

            this.records = new Dictionary<string, object[]>();
            this.getRecords();

            this.recordsDisplay.DisplayMember = "Value";
            this.recordsDisplay.ValueMember = "Key";
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void attachBtn_Click(object sender, EventArgs e)
        {
            odooWebserviceConnector odooConnection;
            try
            {
                odooConnection = new odooWebserviceConnector();
            }
            catch (loginFailedException)
            {
                MessageBox.Show("Login to Odoo server failed or connection isn't configured yet. Check configuration in connection setup.");
                return;
            }

            if (this.recordsDisplay.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a record!");
                return;
            }
            
            DictionaryEntry selectedItem = (DictionaryEntry)this.recordsDisplay.SelectedItem;
            ModelInfo model = (ModelInfo)this.model.SelectedItem;

            object[] argsList = new object[] { new object[] { selectedItem.Key } };
            XmlRpcStruct argsDict = new XmlRpcStruct();
            argsDict.Add("subject", "Attached e-mail from Outlook");
            argsDict.Add("content_subtype", "html");
            argsDict.Add("body", this.mailItem.HTMLBody);
            argsDict.Add("from_outlook", true);
            argsDict.Add("email_from", this.mailItem.SenderEmailAddress);

            List<object> attachments = new List<object>();

            string addinRegistryKeyStr = "SOFTWARE\\Microsoft\\Office\\Outlook\\Addins\\OdooOutlookAddIn.Addin";
            RegistryKey addinRegistryKey = Registry.CurrentUser.OpenSubKey(addinRegistryKeyStr, true);
            string tempFolder = (string)addinRegistryKey.GetValue("TempFolder");
            foreach (Outlook.Attachment attachment in this.mailItem.Attachments)
            {
                string path = tempFolder + "\\" + attachment.FileName;
                attachment.SaveAsFile(path);
                byte[] content = File.ReadAllBytes(path);
                attachments.Add(new object[] { attachment.FileName, content });
                File.Delete(path);
            }

            argsDict.Add("attachments", attachments.ToArray());

            string name = (string)selectedItem.Value;

            try
            {
                odooConnection.execute_kw(model.modelName, "message_post", argsList, argsDict);
            }
            catch (Exception)
            {
                argsDict.Remove("from_outlook"); // make compatible with odoo that doesn't have our module installed
                odooConnection.execute_kw(model.modelName, "message_post", argsList, argsDict);
            }

            this.Close();
        }

        private void model_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.populateDisplay();
        }

        private void populateDisplay()
        {
            ModelInfo model = (ModelInfo)this.model.SelectedItem;
            this.recordsDisplay.Items.Clear();
            foreach (XmlRpcStruct record in this.records[model.displayName])
            {
                int id = (int)record["id"];
                string display = (string)Sync.checkIfBool(record[model.displayField]);

                this.recordsDisplay.Items.Add(new DictionaryEntry(id, display));
            }
        }

        private void searchBar_TextChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(this.searchBar.Text))
            {
                this.populateDisplay();
            }
            else
            {
                this.recordsDisplay.Items.Clear();
                ModelInfo model = (ModelInfo)this.model.SelectedItem;
                foreach (XmlRpcStruct record in this.records[model.displayName])
                {
                    int id = (int)record["id"];
                    string display = (string)Sync.checkIfBool(record[model.displayField]);

                    int index = display.IndexOf(this.searchBar.Text, StringComparison.InvariantCultureIgnoreCase);

                    if (index != -1)
                    {
                        this.recordsDisplay.Items.Add(new DictionaryEntry(id, display));
                    }
                }
            }
        }
    }

    public class ModelInfo
    {
        public string modelName;
        public string displayField;
        public string displayName;
        public object domain;

        public ModelInfo(string modelName, string displayField, string displayName, object domain = null)
        {
            this.modelName = modelName;
            this.displayField = displayField;
            this.displayName = displayName;
            this.domain = domain != null ? domain : new object[] {};
        }

        public override string ToString()
        {
            return this.displayName;
        }
    }
}
