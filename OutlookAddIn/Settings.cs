﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetOffice;
using NetOffice.Tools;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using NetOffice.OutlookApi.Tools;
using Office = NetOffice.OfficeApi;
using NetOffice.OfficeApi.Enums;
using NetOffice.OfficeApi.Tools;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace odooOutlookSettings
{
    class Settings
    {
        private RegistryKey settings;

        public string server
        {
            get
            {
                return (string)this.settings.GetValue("server");
            }
            set
            {
                this.settings.SetValue("server", value);
            }
        }
        public string db
        {
            get
            {
                return (string)this.settings.GetValue("db");
            }
            set
            {
                this.settings.SetValue("db", value);
            }
        }
        public string user
        {
            get
            {
                return (string)this.settings.GetValue("user");
            }
            set
            {
                this.settings.SetValue("user", value);
            }
        }
        public string pass
        {
            // Rfc2898DeriveBytes parameters to generate AES key:
            // Password: Environment.MachineName + Environment.UserName + Environment.OSVersion.ToString()
            // Salt: Random string found in "salt" file in installation folder
            // Iteration Count: 20 000
            get
            {
                byte[] encrypted;
                try
                {
                    encrypted = Convert.FromBase64String((string)this.settings.GetValue("pass"));
                }
                catch (FormatException)
                {
                    return "";
                }

                string plainText = null;
                try
                {
                    using (AesCryptoServiceProvider myAes = this.getAes())
                    {
                        // Create the streams used for decryption.
                        using (MemoryStream msDecrypt = new MemoryStream(encrypted))
                        {
                            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, myAes.CreateDecryptor(), CryptoStreamMode.Read))
                            {
                                using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                                {
                                    // Read the decrypted bytes from the decrypting stream
                                    // and place them in a string.
                                    plainText = srDecrypt.ReadToEnd();
                                }
                            }
                        }
                    }
                }
                catch (System.Security.Cryptography.CryptographicException)
                {
                    MessageBox.Show("Login information could not be retrieved. Please re-enter your login information in the Connection Settings menu.");
                }
                

                return plainText;
            }
            set
            {
                string plainText = value;
                byte[] encrypted = null;
                using (AesCryptoServiceProvider myAes = this.getAes())
                {
                    // Create the streams used for encryption.
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, myAes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {
                                //Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }
                this.settings.SetValue("pass", Convert.ToBase64String(encrypted));
            }
        }
        public string syncPriority
        {
            get
            {
                return (string)this.settings.GetValue("syncPriority");
            }
            set
            {
                this.settings.SetValue("syncPriority", value);
            }
        }
        public string syncFreq
        {
            get
            {
                return (string)this.settings.GetValue("syncFreq");
            }
            set
            {
                this.settings.SetValue("syncFreq", value);
            }
        }
        public string folderName
        {
            get
            {
                string folderName = this.server + " " + this.db;
                folderName = folderName.Replace('&', ' ').Replace('<', ' ').Replace('>', ' ').Replace("'", " ").Replace('"', ' ').Replace(':', ' ').Replace(';', ' ').Replace('\\', ' ').Replace('/', ' ').Replace('(', ' ').Replace(')', ' ').Replace('+', ' ');
                return folderName.Substring(0,Math.Min(folderName.Length, 250)); // max allowed folder length for IMAP servers
            }
        }
        public string notActivatedWarningIssued
        {
            get
            {
                return (string)this.settings.GetValue("notActivatedWarningIssued");
            }
            set
            {
                this.settings.SetValue("notActivatedWarningIssued", value);
            }
        }
        public string versionNumber
        {
            get
            {
                return (string)this.settings.GetValue("VersionNumber");
            }
            set
            {
                this.settings.SetValue("VersionNumber", value);
            }
        }
        public string installPath
        {
            get
            {
                string addinRegistryKeyStr = "SOFTWARE\\Microsoft\\Office\\Outlook\\Addins\\OdooOutlookAddIn.Addin";
                RegistryKey addinRegistryKey = Registry.CurrentUser.OpenSubKey(addinRegistryKeyStr, true);
                return (string)addinRegistryKey.GetValue("InstallFolder");
            }
        }
        public Boolean autoUpdate
        {
            get
            {
                return (string)this.settings.GetValue("autoUpdate") == "True";
            }
            set
            {
                var store = value ? "True" : "False";
                this.settings.SetValue("autoUpdate", store);
            }
        }

        private AesCryptoServiceProvider getAes()
        {
            string PBKDF2_password = Environment.MachineName + Environment.UserName + Environment.OSVersion.ToString();
            byte[] PBKDF2_salt = System.IO.File.ReadAllBytes(this.installPath + "\\salt");
            Rfc2898DeriveBytes AES_key = new Rfc2898DeriveBytes(PBKDF2_password, PBKDF2_salt, 20000);

            byte[] IV = new byte[16];
            int currentIndex = 0;

            byte[] Salt_Pass = new byte[PBKDF2_salt.Length + PBKDF2_password.Length];
            System.Buffer.BlockCopy(PBKDF2_salt, 0, Salt_Pass, 0, PBKDF2_salt.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(PBKDF2_password), 0, Salt_Pass, PBKDF2_salt.Length, PBKDF2_password.Length);

            for (int i = 0; i < 1000; i++)
            {
                currentIndex = Salt_Pass[currentIndex] % Salt_Pass.Length;
            }
            for (int i = 0; i < 16; i++)
            {
                IV[i] = Salt_Pass[currentIndex];
                currentIndex = Salt_Pass[currentIndex] % Salt_Pass.Length;
            }

            AesCryptoServiceProvider myAes = new AesCryptoServiceProvider();
            myAes.BlockSize = 128;
            myAes.KeySize = 256;
            myAes.Key = AES_key.GetBytes(32);
            myAes.IV = IV;
            myAes.Mode = System.Security.Cryptography.CipherMode.CBC;
            myAes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            return myAes;
        }

        public Settings()
        {
            // create settings
            string registryPath = "SOFTWARE\\Microsoft\\Office\\Outlook\\Addins\\OdooOutlookAddIn.Addin\\Settings";
            this.settings = Registry.CurrentUser.OpenSubKey(registryPath, true);
            if (this.settings == null)
            {
                this.settings = Registry.CurrentUser.CreateSubKey(registryPath);
                if (this.settings == null)
                {
                    throw new Exception("Unable to create settings registry");
                }
            }

            this.createSettingProperty("server");
            this.createSettingProperty("db");
            this.createSettingProperty("user");
            this.createSettingProperty("pass");
            this.createSettingProperty("syncPriority");
            this.createSettingProperty("syncFreq");
            this.createSettingProperty("notActivatedWarningIssued");
            this.createSettingProperty("VersionNumber");
            this.createSettingProperty("autoUpdate", default_value: "True");
        }

        // check if property exists and create it if it doesn't
        private void createSettingProperty(string propertyName, string default_value = "")
        {
            if (this.settings.GetValue(propertyName) == null)
            {
                this.settings.SetValue(propertyName, default_value, Microsoft.Win32.RegistryValueKind.String);
            }
        }
    }
}
