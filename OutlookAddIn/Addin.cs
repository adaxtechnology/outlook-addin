﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;
using NetOffice;
using NetOffice.Tools;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using NetOffice.OutlookApi.Tools;
using Office = NetOffice.OfficeApi;
using NetOffice.OfficeApi.Enums;
using NetOffice.OfficeApi.Tools;
using CookComputing.XmlRpc;
using Microsoft.Win32;
using odooWebservice;
using System.Reflection;
using System.Threading;
using odooOutlookSettings;
using odooOutlookSync;
using System.Net;
using System.Web.Script.Serialization;
using Microsoft.Win32.TaskScheduler;
using System.Security.Principal;

namespace OdooOutlookAddIn
{
    [CustomUI("OdooOutlookAddIn.RibbonUI.xml")]
	public class Addin : Outlook.Tools.COMAddin
    {
        public Thread syncThread;
        public Thread manualSyncThread;
        public Thread setTaskSchedulerThread;

        public static Outlook.Application application;

        #region Odoo
        public void synchronize_click(Office.IRibbonControl control)
        {

            if (this.checkGenuine() == false)
            {
                return;
            }

            odooWebserviceConnector odooConnection;
            try
            {
                odooConnection = new odooWebserviceConnector();
            }
            catch
            {
                MessageBox.Show("Login to Odoo server failed or connection isn't configured yet. Check configuration in connection setup.");
                return;
            }
            try
            {
                if (this.manualSyncThread == null || !this.manualSyncThread.IsAlive)
                {
                    Sync sync = new Sync(odooConnection);
                    this.manualSyncThread = new Thread(() => sync.Synchronize(showMsg:true));
                    this.manualSyncThread.Start();
                }
                else
                {
                    MessageBox.Show("Synchronization is already running.");
                }
            }
            catch (Exception e)
            {
                Logger.LogShowMsgBox("Failed to synchronize. Please try again later or check the connection settings for errors.", e.StackTrace, e.Message);
            }

        }

        public void cron_sync()
        {
            if (this.checkGenuine() == false)
            {
                return;
            }

            TimeSpan waitTime;
            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            if(settings.syncFreq == "1h")
            {
                waitTime = new TimeSpan(1, 0, 0);
            }
            else if(settings.syncFreq == "15m")
            {
                waitTime = new TimeSpan(0, 15, 0);
            }
            else
            {
                return;
            }
            while(true)
            {
                if (this.manualSyncThread == null || !this.manualSyncThread.IsAlive)
                {
                    odooWebserviceConnector odooConnection;
                    try
                    {
                        odooConnection = new odooWebserviceConnector();
                    }
                    catch (loginFailedException)
                    {
                        MessageBox.Show("Login to Odoo server failed or connection isn't configured yet. Check configuration in connection setup.");
                        return;
                    }

                    Sync sync = new Sync(odooConnection);
                    sync.Synchronize(showMsg:false);
                }

                Thread.Sleep(waitTime);
            }
        }

        public void setupConnection_click(Office.IRibbonControl control)
        {
            if (this.checkGenuine() == false)
            {
                return;
            }
            var connectWindow = new connect(this);

            connectWindow.ShowDialog();
        }

        public void attach2Lead_click(Office.IRibbonControl control)
        {
            if (this.checkGenuine() == false)
            {
                return;
            }

            // remember that selection is ONE-indexed because why not. thanks microsoft.
            Outlook.Selection selection = OdooOutlookAddIn.Addin.application.ActiveExplorer().Selection;

            if (selection.Count > 0)
            {
                if (selection[1] is Outlook.MailItem)
                {
                    if (selection.Count == 1)
                    {
                        Attach2Lead attachWindow = new Attach2Lead(selection[1] as Outlook.MailItem, this);
                        attachWindow.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("This action can only be used on one e-mail at a time!");
                    }
                }
                else
                {
                    MessageBox.Show("The selected item must be an e-mail!");
                    return;
                }
            }
            else
            {
                MessageBox.Show("No e-mail is currently selected!");
            }
        }
        #endregion

        #region ribbon UI stuff

        public Addin()
		{
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                var settings = new odooOutlookSettings.Settings();
                this.syncThread = new Thread(new ThreadStart(this.cron_sync));
                if (settings.autoUpdate)
                {
                    this.setTaskSchedulerThread = new Thread(new ThreadStart(this.setTaskScheduler));
                    this.setTaskSchedulerThread.Start();
                }
			    this.OnStartupComplete += new OnStartupCompleteEventHandler(Addin_OnStartupComplete);
                this.OnConnection += new OnConnectionEventHandler(Addin_OnConnection);
			    this.OnDisconnection += new OnDisconnectionEventHandler(Addin_OnDisconnection);
            }
            catch (Exception ex)
            {
                Logger.LogShowMsgBox("An error occured in the Odoo to Outlook plugin startup.", ex.Message, ex.Message, ex.StackTrace, ex.ToString());
            }
        }

		internal Office.IRibbonUI RibbonUI { get; private set; }

        public void setTaskScheduler()
        {
            try
            {
                var settings = new odooOutlookSettings.Settings();
                string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                // Get the service on the local machine
                using (TaskService ts = new TaskService())
                {
                    const string taskName = "Odoo_Outlook_Updater";
                    
                    // Create a new task definition and assign properties
                    TaskDefinition td = ts.NewTask();
                    td.RegistrationInfo.Description = "Odoo Outlook Add-In Updater";

                    td.Principal.LogonType = TaskLogonType.InteractiveToken;
                    td.Principal.RunLevel = TaskRunLevel.LUA;

                    // Create a trigger that will fire one minute after logging in
                    td.Triggers.Add(new LogonTrigger()
                    {
                        Delay = TimeSpan.FromMinutes(1),
                        UserId = user,
                    });


                    // Create an action that will launch updater whenever the trigger fires
                    td.Actions.Add(new ExecAction(settings.installPath + "\\download_new_version.exe"));

                    // Register the task in the root folder
                    ts.RootFolder.RegisterTaskDefinition(taskName, td, TaskCreation.CreateOrUpdate, null, null, TaskLogonType.InteractiveToken);
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
            }
        }

        public void disableTaskScheduler()
        {
            try
            {
                var settings = new odooOutlookSettings.Settings();
                string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                // Get the service on the local machine
                using (TaskService ts = new TaskService())
                {
                    const string taskName = "Odoo_Outlook_Updater";
                    Task existing = ts.GetTask(taskName);

                    if (existing != null)
                    {
                        existing.Definition.Triggers[0].Enabled = false;
                        existing.RegisterChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
            }
        }

        private void Addin_OnConnection(object Application, ext_ConnectMode ConnectMode, object AddInInst, ref Array custom)
        {
            try
            {
                OdooOutlookAddIn.Addin.application = this.Application;
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }
        }

		private void Addin_OnStartupComplete(ref Array custom)
		{

        }

		private void Addin_OnDisconnection(ext_DisconnectMode RemoveMode, ref Array custom)
		{
		}

		public void OnLoadRibonUI(Office.IRibbonUI ribbonUI)
        {
            RibbonUI = ribbonUI;

        }

        private bool checkGenuine()
        {
            return true;
        }

		protected override void OnError(ErrorMethodKind methodKind, System.Exception exception)
		{
            MessageBox.Show("An error occurend in " + methodKind.ToString(), "OdooOutlookAddIn");
		}
        #endregion
    }
}

