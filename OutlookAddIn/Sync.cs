﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;
using NetOffice;
using NetOffice.Tools;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using NetOffice.OutlookApi.Tools;
using Office = NetOffice.OfficeApi;
using NetOffice.OfficeApi.Enums;
using NetOffice.OfficeApi.Tools;
using CookComputing.XmlRpc;
using Microsoft.Win32;
using odooWebservice;
using System.Reflection;
using odooOutlookSettings;
using OdooOutlookAddIn;

namespace odooOutlookSync
{
    class Sync
    {
        odooWebserviceConnector odooConnection = null;

        Outlook.Application outlook = null;

        string idFilter = "[odooID] = {0}";

        private string companyFilter = "[FullName] = '' And [CompanyName] = '{0}'";

        private Dictionary<string, int> countries = new Dictionary<string, int>();
        private Dictionary<string, int> states = new Dictionary<string, int>();

        public Sync(odooWebserviceConnector odooConnection)
        {
            this.outlook = OdooOutlookAddIn.Addin.application;

            this.odooConnection = odooConnection;

            Outlook.MAPIFolder ContactFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderContacts);
            Outlook.MAPIFolder CalendarFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);

            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            try
            {
                ContactFolder = ContactFolder.Folders[settings.folderName];
            }
            catch
            {
                // folder doesn't exist yet
                ContactFolder = ContactFolder.Folders.Add(settings.folderName);
                ContactFolder.ShowAsOutlookAB = true;
            }
            if (ContactFolder.UserDefinedProperties.Find("odooID") == null)
            {
                ContactFolder.UserDefinedProperties.Add("odooID", OlUserPropertyType.olInteger, true);
            }


            try
            {
                CalendarFolder = CalendarFolder.Folders[settings.folderName];
            }
            catch
            {
                // folder doesn't exist yet
                CalendarFolder = CalendarFolder.Folders.Add(settings.folderName);
            }

            if (CalendarFolder.UserDefinedProperties.Find("odooID") == null)
            {
                CalendarFolder.UserDefinedProperties.Add("odooID", OlUserPropertyType.olInteger, true);
            }
        }

        public void Synchronize(bool showMsg = true)
        {
            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();
            try
            {
                this.initStatesCountries();

                if (settings.syncPriority == "Outlook")
                {
                    this.syncContactsOutlook();
                    this.syncCalendarOutlook();
                }
                else
                {
                    this.syncContactsOdoo();
                    this.syncCalendarOdoo();
                }
                if (showMsg)
                {
                    MessageBox.Show("Synchronization Completed!");
                }
            }
            catch (Exception ex)
            {
                if (showMsg)
                {
                    Logger.LogShowMsgBox("Failed to synchronize. Please try again later or check the connection settings for errors.", ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace + "\n" + ex.ToString());
                }
                else
                {
                    Logger.Log(ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace + "\n" + ex.ToString());
                }
            }
        }

        public void initStatesCountries()
        {

            XmlRpcStruct argsDict = new XmlRpcStruct();
            argsDict.Add("fields", new object[] { "id", "name" });
            object[] countryList = (object[])this.odooConnection.execute_kw("res.country", "search_read", new object[] { new object[] { } }, argsDict);
            object[] stateList = (object[])this.odooConnection.execute_kw("res.country.state", "search_read", new object[] { new object[] { } }, argsDict);
            string c, s;

            foreach (XmlRpcStruct country in countryList)
            {
                c = (string)Sync.checkIfBool(country["name"]);
                if (c.Length > 0 && !this.countries.ContainsKey(c))
                {
                    this.countries.Add(c, (int)country["id"]);
                }
            }
            foreach (XmlRpcStruct state in stateList)
            {
                s = (string)Sync.checkIfBool(state["name"]);
                if (s.Length > 0 && !this.states.ContainsKey(s))
                {
                    this.states.Add(s, (int)state["id"]);
                }
            }
        }

        public int syncCalendarOutlook()
        {
            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();
            Outlook.MAPIFolder CalendarFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar).Folders[settings.folderName];

            Outlook._Items appointments = CalendarFolder.Items;

            if (appointments.Count == 0)
            {
                // if no appointments in Outlook then download them instead
                return this.syncCalendarOdoo();
            }

            try
            {
                int[] odooIDs;
                object result;
                
                result = this.odooConnection.execute_kw("calendar.event", "search", new object[] { new object[] { } }, null);
                
                try
                {
                    odooIDs = (int[])result;
                }
                catch (Exception)
                {
                    // dirty hack to deal with the fucking Google Calendar module. The module changes some ids to strings so fuck those ids.
                    var temp = (object[])result;
                    var tempIds = new List<int>();
                    for (int i = 0; i < temp.Length; i++)
                    {
                        try
                        {
                            tempIds.Add((int)temp[i]);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    odooIDs = tempIds.ToArray();
                }

                var updatedIds = new List<int>();

                foreach (Outlook.AppointmentItem appointment in appointments)
                {
                    if (appointment.UserProperties["odooID"] == null && Array.IndexOf(odooIDs, (int)appointment.UserProperties["odooID"].Value) == -1)
                    {
                        appointment.UserProperties["odooID"].Delete();
                    }

                    this.saveEventOdoo(appointment, appointments);

                    updatedIds.Add((int)appointment.UserProperties["odooID"].Value);
                }

                this.syncCalendarOdoo(omitIds: updatedIds);
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }

            return 0;
        }

        public int syncCalendarOdoo(List<int> omitIds = null)
        {
            if (this.odooConnection == null)
            {
                return -1;
            }

            object[] argsList;
            
            if (omitIds != null)
            {
                argsList = new object[] { new object[] { new object[] { "id", "not in", omitIds.ToArray() } } };
            }
            else
            {
                omitIds = new List<int>();
                argsList = new object[] { new object[] { } };
            }

            XmlRpcStruct argsDict = new XmlRpcStruct();
            argsDict.Add("fields", new object[] { "id", "name", "location", "start", "stop", "allday", "description" });
            object[] calendarEventList = (object[])this.odooConnection.execute_kw("calendar.event", "search_read", argsList, argsDict);

            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            Outlook.MAPIFolder CalendarFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar).Folders[settings.folderName];

            Outlook._Items appointments = CalendarFolder.Items;

            var updatedIDs = new List<int>();
            try
            {
                foreach (XmlRpcStruct calendarEvent in calendarEventList)
                {
                    int id;
                    try
                    {
                        id = (int)calendarEvent["id"]; // this will throw an exception with the Google Calendar module because it modifies the id to a string
                    }
                    catch (Exception)
                    {
                        // quietly omit
                        continue;
                    }
                    string name = (string)Sync.checkIfBool(calendarEvent["name"]);
                    string location = (string)Sync.checkIfBool(calendarEvent["location"]);
                    string startDate = (string)Sync.checkIfBool(calendarEvent["start"]);
                    string stopDate = (string)Sync.checkIfBool(calendarEvent["stop"]);
                    string description = (string)Sync.checkIfBool(calendarEvent["description"]);
                    bool allday = (bool)calendarEvent["allday"];

                    this.saveEventOutlook(id, name, location, startDate, stopDate, description, allday, appointments);

                    updatedIDs.Add(id);
                }
                foreach (Outlook.AppointmentItem appointment in appointments)
                {
                    if (appointment.UserProperties["odooID"] == null)
                    {
                        this.saveEventOdoo(appointment, appointments);
                        continue;
                    }
                    var id = (int)appointment.UserProperties["odooID"].Value;
                    if (!(updatedIDs.Contains(id)) && !omitIds.Contains(id))
                    {
                        // Event previously existed in Odoo but doesn't anymore so delete it.
                        appointment.Delete();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }

            return 0;
        }

        public int syncContactsOutlook()
        {
            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            Outlook.MAPIFolder ContactFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderContacts).Folders[settings.folderName];

            Outlook._Items contacts = ContactFolder.Items;

            if (contacts.Count == 0)
            {
                // if no contacts in Outlook then download them instead
                return this.syncContactsOdoo();
            }

            try
            {
                int[] odooIDs = (int[])this.odooConnection.execute_kw("res.partner", "search", new object[] { new object[] { } }, null);
                var updatedIDs = new List<int>();

                foreach (Outlook.ContactItem contact in contacts)
                {
                    if (contact.UserProperties["odooID"] != null && Array.IndexOf(odooIDs, (int)contact.UserProperties["odooID"].Value) == -1)
                    {
                        // contact was previously in Odoo but not anymore
                        contact.UserProperties["odooID"].Delete();
                    }

                    this.saveContactOdoo(contact, contacts);

                    updatedIDs.Add((int)contact.UserProperties["odooID"].Value);
                }

                this.syncContactsOdoo(omitIds: updatedIDs);
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }

            return 0;
        }

        public int syncContactsOdoo(List<int> omitIds = null)
        {
            if (this.odooConnection == null)
            {
                return -1;
            }

            object[] argsList;

            if (omitIds != null)
            {
                argsList = new object[] { new object[] { new object[] { "id", "not in", omitIds.ToArray() } } };
            }
            else
            {
                argsList = new object[] { new object[] { } };
                omitIds = new List<int>();
            }

            XmlRpcStruct argsDict = new XmlRpcStruct();
            argsDict.Add("fields", new object[] { "id", "name", "phone", "mobile", "fax", "email", "parent_id", "is_company", "street", "city", "state_id", "zip", "country_id" });
            object[] partnerList = (object[])this.odooConnection.execute_kw("res.partner", "search_read", argsList, argsDict);

            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            Outlook.MAPIFolder ContactFolder = (Outlook.MAPIFolder)this.outlook.Session.GetDefaultFolder(OlDefaultFolders.olFolderContacts).Folders[settings.folderName];

            Outlook._Items contacts = ContactFolder.Items;

            List<int> updatedIDs = new List<int>();

            try
            {
                foreach (XmlRpcStruct partner in partnerList)
                {
                    int id = (int)partner["id"];
                    string name = (string)Sync.checkIfBool(partner["name"]);
                    string phone = (string)Sync.checkIfBool(partner["phone"]);
                    string mobile = (string)Sync.checkIfBool(partner["mobile"]);
                    string fax = (string)Sync.checkIfBool(partner["fax"]);
                    string email = (string)Sync.checkIfBool(partner["email"]);
                    string company = Sync.m2oGetString(partner["parent_id"]);
                    bool isCompany = (bool)partner["is_company"];
                    string street = (string)Sync.checkIfBool(partner["street"]);
                    string city = (string)Sync.checkIfBool(partner["city"]);
                    string state = (string)Sync.m2oGetString(partner["state_id"]);
                    string zipcode = (string)Sync.checkIfBool(partner["zip"]);
                    string country = (string)Sync.m2oGetString(partner["country_id"]);

                    this.saveContactOutlook(id, name, phone, mobile, fax, email, company, isCompany, street, city, state, zipcode, country, contacts);
                    updatedIDs.Add(id);
                }
                foreach(Outlook.ContactItem contact in contacts)
                {
                    if (contact.UserProperties["odooID"] == null)
                    {
                        this.saveContactOdoo(contact, contacts);
                        continue;
                    }
                    var id = (int)contact.UserProperties["odooID"].Value;
                    if (!(updatedIDs.Contains(id)) && !omitIds.Contains(id))
                    {
                        // Contact previously existed in Odoo but doesn't anymore so delete it.
                        contact.Delete();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }

            return 0;
        }

        private void saveContactOdoo(Outlook.ContactItem contact, Outlook._Items contacts)
        {
            // create new contact in odoo
            XmlRpcStruct createValues = new XmlRpcStruct();
            if (string.IsNullOrEmpty(contact.FullName) && !string.IsNullOrEmpty(contact.CompanyName))
            {
                createValues.Add("name", contact.CompanyName);
                createValues.Add("is_company", true);
            }
            else
            {
                createValues.Add("name", contact.FullName);
            }

            createValues.Add("email", contact.Email1Address);
            createValues.Add("phone", contact.BusinessTelephoneNumber);
            createValues.Add("mobile", contact.MobileTelephoneNumber);
            createValues.Add("fax", contact.BusinessFaxNumber);

            createValues.Add("street", contact.BusinessAddressStreet);
            createValues.Add("city", contact.BusinessAddressCity);

            if (contact.BusinessAddressState.Length > 0)
            {
                if (this.states.ContainsKey(contact.BusinessAddressState))
                {
                    createValues.Add("state_id", this.states[contact.BusinessAddressState]);
                }
            }
            createValues.Add("zip", contact.BusinessAddressPostalCode);

            if (contact.BusinessAddressCountry.Length > 0)
            {
                if (this.countries.ContainsKey(contact.BusinessAddressCountry))
                {
                    createValues.Add("country_id", this.countries[contact.BusinessAddressCountry]);
                }
            }

            object[] argsList = new object[] { createValues };

            // check if parent company exists and assign it (create if necessary)
            if (contact.CompanyName.Length != 0 && contact.FullName != "")
            {
                int companyID = 0;
                string _companyFilter = string.Format(this.companyFilter, contact.CompanyName);
                Outlook._Items contactsFound = contacts.Restrict(_companyFilter);
                if (contactsFound.Count == 0)
                {
                    // create company
                    XmlRpcStruct companyCreate = new XmlRpcStruct();
                    companyCreate.Add("name", contact.CompanyName);
                    companyCreate.Add("is_company", true);
                    companyID = (int)this.odooConnection.execute_kw("res.partner", "create", new object[] { companyCreate });
                }
                else
                {
                    companyID = (int)(contactsFound.GetFirst() as Outlook.ContactItem).UserProperties["odooID"].Value;
                }
                createValues.Add("parent_id", companyID);
            }

            if (contact.UserProperties["odooID"] == null)
            {
                contact.UserProperties.Add("odooID", OlUserPropertyType.olInteger, false).Value = (int)this.odooConnection.execute_kw("res.partner", "create", argsList, null);
            }
            else
            {
                argsList = new object[] { new object[] { (int)contact.UserProperties["odooID"].Value }, createValues };
                this.odooConnection.execute_kw("res.partner", "write", argsList, null);
            }

            contact.Save();
        }

        private Outlook.ContactItem saveContactOutlook(int id, string name, string phone, string mobile, string fax, string email, string company, bool isCompany, string street, string city, string state, string zipcode, string country, Outlook._Items contacts)
        {
            string _filter = string.Format(this.idFilter, id.ToString());
            Outlook._Items contactsFound = contacts.Restrict(_filter);

            Outlook.ContactItem contact;

            if (contactsFound.Count == 0)
            {
                contact = contacts.Add(OlItemType.olContactItem) as Outlook.ContactItem;
                contact.UserProperties.Add("odooID", OlUserPropertyType.olInteger, true).Value = id;
            }
            else
            {
                contact = contactsFound.GetFirst() as Outlook.ContactItem;
            }
            if (isCompany)
            {
                contact.CompanyName = name;
            }
            else
            {
                contact.FullName = name;
                contact.CompanyName = company;
            }

            contact.Email1Address = email;
            contact.Email1AddressType = "SMTP";

            contact.BusinessTelephoneNumber = phone;
            contact.MobileTelephoneNumber = mobile;
            contact.BusinessFaxNumber = fax;

            contact.BusinessAddressStreet = street;
            contact.BusinessAddressCity = city;
            contact.BusinessAddressState = state;
            contact.BusinessAddressPostalCode = zipcode;
            contact.BusinessAddressCountry = country;

            try
            {
                contact.Save();
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, e.StackTrace);
                Exception x = e.InnerException;
                Logger.Log(x.Message, x.StackTrace);
            }

            return contact;
        }

        private void saveEventOdoo(Outlook.AppointmentItem appointment, Outlook._Items appointments)
        {
            // create new contact in odoo
            XmlRpcStruct createValues = new XmlRpcStruct();
            createValues.Add("name", appointment.Subject);
            createValues.Add("location", appointment.Location);
            createValues.Add("description", appointment.Body);
            if (appointment.AllDayEvent)
            {
                createValues.Add("start", appointment.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                if (appointment.End.AddDays(-1) < appointment.Start) // weird logic to deal with how outlook and odoo save allday events
                {
                    createValues.Add("stop", appointment.End.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else
                {
                    createValues.Add("stop", appointment.End.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }
            else
            {
                createValues.Add("start", appointment.StartUTC.ToString("yyyy-MM-dd HH:mm:ss"));
                createValues.Add("stop", appointment.EndUTC.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            createValues.Add("allday", appointment.AllDayEvent);
            var argsList = new object[] { createValues };

            if (appointment.UserProperties["odooID"] == null)
            {
                appointment.UserProperties.Add("odooID", OlUserPropertyType.olInteger, true).Value = (int)this.odooConnection.execute_kw("calendar.event", "create", argsList, null);
            }
            else
            {
                argsList = new object[] { new object[] { (int)appointment.UserProperties["odooID"].Value }, createValues };
                this.odooConnection.execute_kw("calendar.event", "write", argsList, null);
            }

            appointment.Save();
        }

        private Outlook.AppointmentItem saveEventOutlook(int id, string name, string location, string startDate, string stopDate, string description, bool allday, Outlook._Items appointments)
        {
            DateTime start = new DateTime(
                Int32.Parse(startDate.Substring(0, 4)), // year
                Int32.Parse(startDate.Substring(5, 2)), // month
                Int32.Parse(startDate.Substring(8, 2)), // day
                Int32.Parse(startDate.Substring(11, 2)), // hour
                Int32.Parse(startDate.Substring(14, 2)), // minute
                Int32.Parse(startDate.Substring(17, 2)) // second
            );
            DateTime stop = new DateTime(
                Int32.Parse(stopDate.Substring(0, 4)), // year
                Int32.Parse(stopDate.Substring(5, 2)), // month
                Int32.Parse(stopDate.Substring(8, 2)), // day
                Int32.Parse(stopDate.Substring(11, 2)), // hour
                Int32.Parse(stopDate.Substring(14, 2)), // minute
                Int32.Parse(stopDate.Substring(17, 2)) // second
            );

            string _filter = string.Format(this.idFilter, id.ToString());
            Outlook._Items appointmentsFound = appointments.Restrict(_filter);

            Outlook.AppointmentItem appointment;
            if (appointmentsFound.Count == 0)
            {
                appointment = appointments.Add(OlItemType.olAppointmentItem) as Outlook.AppointmentItem;
                appointment.UserProperties.Add("odooID", OlUserPropertyType.olInteger, true).Value = id;
            }
            else
            {
                appointment = appointmentsFound.GetFirst() as Outlook.AppointmentItem;
            }

            appointment.Subject = name;
            appointment.Location = location;
            appointment.StartUTC = start;
            appointment.EndUTC = stop;
            appointment.Body = description;
            appointment.AllDayEvent = allday;

            appointment.Save();

            return appointment;
        }

        static public object checkIfBool(object value)
        {
            if (value.GetType() == typeof(bool))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        static public string m2oGetString(dynamic m2oValue)
        {
            if (m2oValue.GetType() == typeof(bool))
            {
                return "";
            }
            else
            {
                return (string) m2oValue[1];
            }
        }
    }
}
